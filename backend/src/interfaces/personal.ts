import { Document } from 'mongoose'

interface IPersonal extends Document {
 page: string,
 title: string,
 content: string,
 imageUrl: string
}

export default IPersonal;
