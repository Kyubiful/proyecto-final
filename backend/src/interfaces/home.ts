import { Document } from 'mongoose';

interface IHome extends Document {
  page: string,
  description: {
    title: string,
    content: string,
    imageUrl: string
  },
  personalCoach:{
    title: string,
    content: string,
    imageUrl: string
  },
  executiveCoach:{
    title: string,
    content: string,
    imageUrl: string
  }
}

export default IHome;
