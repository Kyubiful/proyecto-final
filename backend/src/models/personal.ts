import IPersonal from '../interfaces/personal';
import { model, Schema } from 'mongoose';

const personalSchema = new Schema ({
  page: { type: String, required: true, trim: true },
  title: { type: String, required: true, trim: true },
  content: { type: String, required: true, trim: true },
  imageUrl: { type: String, required: true, trim: true }
});

export default model<IPersonal>('Personal',personalSchema);
