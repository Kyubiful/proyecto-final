import IHome from '../interfaces/home';
import { model, Schema } from 'mongoose';

const homeSchema = new Schema ({
  page: String,
  description: {
    title: { type: String, required: true, trim: true },
    content: { type: String, required: true, trim: true },
    imageUrl: { type: String, required: true, trim: true }
  },
  personalCoach: {
    title: { type: String, required: true, trim: true },
    content: { type: String, required: true, trim: true },
    imageUrl: { type: String, required: true, trim: true }
  },
  executiveCoach: {
    title: { type: String, required: true, trim: true },
    content: { type: String, required: true, trim: true },
    imageUrl: { type: String, required: true, trim: true }
  }
});

export default model<IHome>('Home',homeSchema);
