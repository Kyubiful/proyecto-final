import { Request, Response } from 'express';
import homeModel from '../models/home';

const homeController = {
  updateHome: async (req: Request, res: Response) => {
    let homeRequest = await req.body;
    let homeRequestObject: Object = {
      description: { 
	title: homeRequest.description.title, 
	content: homeRequest.description.content, 
	imageUrl: homeRequest.description.imageUrl 
      }, 
      personalCoach: { 
	title: homeRequest.personalCoach.title, 
	content: homeRequest.personalCoach.content, 
	imageUrl: homeRequest.personalCoach.imageUrl
      },
      executiveCoach: {
	title: homeRequest.executiveCoach.title, 
	content: homeRequest.executiveCoach.content, 
	imageUrl:homeRequest.executiveCoach.imageUrl
      }
    };
    await homeModel.findOneAndUpdate({page:'Home'}, homeRequestObject, null, (err, replaced) => {
      if(err) return res.status(500).send({message: err});
      if(!replaced) return res.status(404).send({message: 'No se ha podido actualizar'});
      if(replaced) return res.status(200).send({message: replaced});
    });
  },
  getHome: async (req: Request, res: Response) => {
    await homeModel.find({page:'Home'}, (err: any, homeData: any) => {
      if(err) return res.status(500).send({message: 'getHome error'});
      if(!homeData) return res.status(404).send({message: 'No se ha podido obtener Home'});
      if(homeData) return res.status(200).send(homeData);
    });
  }
}

export default homeController;
