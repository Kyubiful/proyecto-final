import { Request, Response } from 'express';
import personalModel from '../models/personal';

const personalController = {
  updatePersonal: async (req: Request, res: Response) => {
    let personalRequest = await req.body;
    let personalObject: Object = {
      title: personalRequest.title,
      content: personalRequest.content,
      imageUrl: personalRequest.imageUrl
    }
    await personalModel.findOneAndUpdate({page: 'Personal'}, personalObject, null, (err, replaced) => {
      if(err) return res.status(500).send({message: err});
      if(!replaced) return res.status(404).send({message: 'No se ha podido actualizar'});
      if(replaced) return res.status(200).send(replaced);
    });
  },
  getPersonal: async (req: Request, res: Response) => {
    await personalModel.find({page: 'Personal'}, (err: any, personalData: any) => {
      if(err) return res.status(500).send({message: 'getPersonalCoach error'});
      if(!personalData) return res.status(404).send({message: 'No se ha podido obtener Personal Coach'});
	if(personalData) return res.status(200).send(personalData);
    });
  }
};

export default personalController;
