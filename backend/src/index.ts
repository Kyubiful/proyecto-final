import app from './app';

import backConfig from './config/express.config';

var main = () => {

  app.listen(backConfig.port, () => {
    console.log('Express funcionando correctamente en el puerto '+backConfig.port)
  });

}

main();