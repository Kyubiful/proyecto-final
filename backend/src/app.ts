import express, { Application } from 'express';
import dbConnection from './database';

// Crear app express
const app: Application = express();

// Conexion con la base de datos
dbConnection();

//Middlewares
app.use(express.urlencoded({extended: false}));
app.use(express.json());

// Cargar archivos de ruta
import homeRoutes from './routes/home';
import personalRoutes from './routes/personal';
// CORS
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
  res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
  next();
});
// Rutas

app.use('/home', homeRoutes);
app.use('/personal', personalRoutes);

// Exportar
export default app;
