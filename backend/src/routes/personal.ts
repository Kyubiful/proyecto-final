import express from 'express';
import personalController from '../controllers/personal';

const router = express.Router();

router.post('/update', personalController.updatePersonal);
router.get('/get', personalController.getPersonal);

export default router;
