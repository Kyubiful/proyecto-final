import express from 'express';
import homeController from '../controllers/home';

const router = express.Router();

router.post('/update', homeController.updateHome);
router.get('/get', homeController.getHome);

export default router;
