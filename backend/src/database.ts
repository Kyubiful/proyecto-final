import mongoose from 'mongoose';
import dbConfig from './config/mongodb.config';

const config: object = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false
}

const dbConnection = async () => {

  await mongoose.connect('mongodb://'+dbConfig.url+':'+dbConfig.port+'/'+dbConfig.dbName, config)
    .then(db => console.log('Conexión con la base de datos '+mongoose.connection.name+ ' en el puerto '+mongoose.connection.port+' establecida correctamente'))
    .catch(err => console.log(err));

}

export default dbConnection;
