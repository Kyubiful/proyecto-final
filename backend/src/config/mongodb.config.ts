const config = {
  url: 'localhost',
  port: 27017,
  dbName: 'coach-web'
}

export default config;